template<class GV>
void novlp_solvers (const GV& gv, const int i)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::ConformingDirichletConstraints CON;    // constraints class
  CON con(gv);
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem,con);
  gfs.name("solution");
  //con.compute_ghosts(gfs);
  BCTypeParam bctype; // boundary condition type
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  cc.clear();
  Dune::PDELab::constraints(bctype,gfs,cc); // assemble constraints

  // some informations
  gfs.update();
  auto nrofdof =  gfs.globalSize();
  std::cout << "rank " << gv.comm().rank() << " number of DOF = " << nrofdof << std::endl;
  nrofdof = gv.comm().sum(nrofdof);
  if (gv.comm().rank()==0)
    std::cout << "number of DOF " << nrofdof << std::endl;

  // <<<3>>> Make grid operator space
  typedef ExampleLocalOperator<BCTypeParam> LOP;
  LOP lop(bctype);
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  MBE mbe(27); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO; 
  GO go(gfs,cc,gfs,cc,lop,mbe);

  // <<<4>>> Make FE function with BC
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0); // initial value
  typedef BCExtension<GV,Real> G;      // boundary value + extension
  G g(gv);
  Dune::PDELab::interpolate(g,gfs,u);  // interpolate coefficient vector

  // <<<5>>> Select a linear solver backend and solve linear problem
  Dune::PDELab::DefaultTimeSource source;
  double start=source();

  if(i==0){
    // *TODO* use novlp linear solver here and uncomment below
    // do NOT use solvers with Jacobi preconditioner, they are now broken!
    // typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    // SLP slp(go,ls,u,1e-10);
    // slp.apply();
  }



  if (gv.comm().rank() == 0)
    std::cout<<"Total computation time was "<<source()-start
             <<" seconds."<<std::endl;

  // <<<6>>> graphical output
  Dune::VTKWriter<GV> vtkwriter(gv);
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
  if(i==0){
    vtkwriter.pwrite("novlp_solver","vtk","",Dune::VTK::appendedraw);}
}

