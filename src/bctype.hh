#include<dune/common/fvector.hh>

#include<dune/pdelab/common/function.hh>

class BCTypeParam
  : public Dune::PDELab::DirichletConstraintsParameters
{
public:

  template<typename I>
  bool isDirichlet(
                   const I & intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
                   ) const
  {
    return true;  // Dirichlet b.c. on all boundaries
  }

};
