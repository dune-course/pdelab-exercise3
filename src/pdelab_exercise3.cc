#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include<iostream>

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/timer.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/common/gridfactory.hh>
#include<dune/grid/yaspgrid.hh>
#if HAVE_UG 
#include <dune/grid/uggrid.hh>
#endif
#if HAVE_ALUGRID
#include<dune/grid/alugrid.hh>
#include<dune/grid/alugrid/2d/alugrid.hh>
#endif
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/superlu.hh>
#include<dune/istl/paamg/amg.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/common/benchmarkhelper.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/stationary/linearproblem.hh>

#include"bctype.hh"
#include"bcextension.hh"
#include"local_operator.hh"
#include"seq_solvers.hh"
#include"novlp_solvers.hh"
#include"ovlp_solvers.hh"

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{

    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv); 

    if (argc!=2)
      {
 	if(helper.rank()==0)
 	  std::cout << "usage: ./pdelab_exercise3 <verbosity>" << std::endl;
 	return 1;
      }
           
    int verbosity = 2;
    //    sscanf(argv[1],"%d",&verbosity);

    Dune::FieldVector<double,3> L(1.0);
    int nr_of_elements = 10;
    Dune::array<int,3> N(Dune::fill_array<int,3>(nr_of_elements));
    std::bitset<3> periodic(false);
    int overlap0=0;
    int overlap1=1;
    typedef Dune::YaspGrid<3>::LeafGridView GV_Cube_yasp;
    Dune::YaspGrid<3> novlp_yasp(L,N,periodic,overlap0);
    Dune::YaspGrid<3> ovlp_yasp(L,N,periodic,overlap1);
   
    const GV_Cube_yasp& gv_novlp_3d_yasp = novlp_yasp.leafGridView();
    const GV_Cube_yasp& gv_ovlp_3d_yasp = ovlp_yasp.leafGridView();
	
//    typedef Dune::ALUCubeGrid<3,3> GridType;
//    // read gmsh file
//    Dune::GridFactory<GridType> factory;
//    if (helper.rank()==0)
//      Dune::GmshReader<GridType>::read(factory,"./grids/complex.msh",true,true);
//    GridType *grid=factory.createGrid();
//    grid->loadBalance();
//    typedef GridType::LeafGridView GV;
//    const GV& gv_cube_alu=grid->leafView();

    if(helper.size()==1){
      std::cout << "SEQ_BCGS_SSOR" << std::endl;
      //the second parameter chooses between several different solvers in the same file,
      //look at the source code of *_solvers.hh.
      seq_solvers(gv_ovlp_3d_yasp,0);
      std::cout << std::endl;
    }

    if(helper.size()==1){
      //std::cout << "other seq solver" << std::endl;
      //seq_solvers(gv_ovlp_3d_yasp,1);
      //std::cout << std::endl;
    }

    if(helper.rank()==0){
      std::cout << "add ovlp solver here" << std::endl;}
    //*TODO* uncomment below to use ovlp solver
    //ovlp_solvers(gv_ovlp_3d_yasp,0);
    if(helper.rank()==0){
      std::cout << std::endl;}

    if(helper.rank()==0){
      std::cout << "add novlp solver here" << std::endl;}
    //*TODO* uncomment below to use novlp solver
    //novlp_solvers(gv_novlp_3d_yasp,0);
    if(helper.rank()==0){
      std::cout << std::endl;}


  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
